import axios from "axios";

const instance = axios.create({
  baseURL: "http:localhost:3000",
});

function delay(time: number) {
  return new Promise((reslove, reject) => {
    setTimeout(() => {
      reslove(true);
    }, time);
  });
}

instance.interceptor.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default instance;
