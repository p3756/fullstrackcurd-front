import { defineStore } from "pinia";
import { computed, ref } from "vue";

export const useMessageStore = defineStore("loading", () => {
  const isShow = ref(false);
  const message = ref("");

  function showError(text: string) {
    message.value = text;
    isShow.value = true;
  }
  return { isShow, message, showError };
});
